import Vue from 'vue'
import VueRouter from 'vue-router'
import StartScreen from "../components/StartScreen";
import QuizScreen from "../components/QuizScreen";
import hey from "@/components/ResultsScreen";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Start',
    alias:'/Start',
    component: StartScreen
  },
  {
    path: '/quiz',
    name: 'Quiz',
    component: QuizScreen
  },
  {
    path:'/result',
    name:'result',
    component: hey
  }
]

const router = new VueRouter({
  routes
})

export default router
