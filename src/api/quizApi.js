const URL = 'https://opentdb.com/';

export const quizApi = {
    async getQuestions(numberQuestions, category, difficulty){
        const questions = await fetch(`${URL}api.php?amount=${numberQuestions}&category=${category}&difficulty=${difficulty}`)
            .then(response => response.json());

        return questions;
    },
    async getCategories(){
        const categories = await fetch(`${URL}api_category.php`)
            .then(response => response.json());

        return categories;
    }
}