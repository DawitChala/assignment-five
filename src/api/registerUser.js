const URL = 'https://noroff-quiz-api.herokuapp.com';
const API_KEY = 'banananannannanannakake'

export const registerUser = {
    async register(username){

        const userArray = await fetch(`${URL}/trivia?username=${username}`)
            .then(response => response.json())

        const reqOptions = {
            method:'POST',
            headers: {
                'X-API-Key':API_KEY,
                'Content-Type' : 'application/json'
            },
            body:JSON.stringify({
                username,
                highScore: 0
            })
        }

        if(userArray.length===0){
            return await fetch(`${URL}/trivia`,reqOptions)
                .then(response => response.json());
        }
        else {
            return userArray[0];
        }
    },
    async updateHighScore(username,highScore){
        const reqOptions = {
            method:'PATCH',
            headers: {
                'X-API-Key':API_KEY,
                'Content-Type' : 'application/json'
            },
            body:JSON.stringify({
                highScore: highScore
            })
        }

        const user = await fetch(`${URL}/trivia/${username}`,reqOptions)
            .then(response => response.json())
        console.log(reqOptions)
        return user;
    }
}