import Vue from 'vue'
import  Vuex from 'vuex'
import {registerUser} from "../api/registerUser";
import {quizApi} from "../api/quizApi";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        username: '',
        questions: [],
        error: '',
        points: 0,
        highScore:0,
        categoryId:'',
        difficulty:''
    },
    mutations: {
        addAnswer(state,{questionIndex,answer}){

            state.questions[questionIndex]['answer'] = answer;
        }
        ,
        setHighScore(state,payload){
            state.highScore = payload;
        },
        incrementPoints(state){
            state.points++;
        },
        setUsername(state, payload){
            state.username = payload;
        },
        setError(state,payload){
            state.error = payload;
        },
        setQuestions(state, payload){
            state.questions = payload;
        },
        //clears state
        setDefaultState(state){
            state.username = '',
                state.questions = [],
                state.error = '',
                state.points = 0,
                state.highScore = 0,
                state.categoryId='',
                state.difficulty=''
        },
        setChosenConfig(state,{categoryId,difficulty}){
            state.categoryId = categoryId;
            state.difficulty = difficulty;
        }

    },
    getters: {
        returnQuestions: state => {
            return state.questions
        },
        returnScore: state => {
            return state.points
        },
        returnDifficulty: state =>{
            return state.difficulty;
        },
        returnCategoryId: state =>{
            return state.categoryId;
        },
        returnUserId: state => {
            return state.username
        }
    },
    actions: {
        async register({commit},name){
            try {

                const user = await registerUser.register(name);
                commit('setUsername',user.id);
                commit('setHighScore',user.highScore);

            }
            catch (error){
                console.error(error);
            }
        },

        async getTriviaQuestions({commit}, {numberOfQuestions, categoryId, difficulty}){
            try {
                const trivia_questions = await quizApi.getQuestions(numberOfQuestions, categoryId, difficulty.toLowerCase());
                commit('setQuestions', trivia_questions.results);
            }
            catch (error){
                console.error(error);
            }
        },

        async updateHighscore({state,commit}){
            if(state.points<state.highScore) return;
            commit('setHighScore',state.points);
            await registerUser.updateHighScore(state.username,state.highScore);
        },

        updateScore({commit}){
           commit('incrementPoints');
        },
        addAnswerToQuestion({commit},{questionIndex,answer}){

            commit('addAnswer',{questionIndex,answer})
        },
        setState({state, commit}){
          commit( 'setDefaultState',state);
        },
        setCategoryIdAndDifficulty({commit},payload){
            commit('setChosenConfig',payload);

        }
    }

})